package com.vbandrew.rtmp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore;

//import java.util.ArrayList;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.LOG;


import org.json.JSONArray;
import org.json.JSONException;



public class rtmp extends CordovaPlugin {
    private static String LOG_TAG = "rtmp_MainActivity";
    private static int CAMERA = 1;
    public static String STREAM_APP = "multialarma";
    public static String STREAM_ID  = "";


    CallbackContext callbackContext;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /*@Override
    protected void onResume(){
        super.onResume();
        LOG.d(LOG_TAG, "Xxxxxxxxxx ON RESUME xxxxxxxxxxxX");
    }*/

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;

        if (action.equals("init")) {
            STREAM_ID = args.getString(0);
            LOG.d(LOG_TAG, "STREAM_ID: "+STREAM_ID);

            callbackContext.success(STREAM_ID);
            return true;
        }
        else if (action.equals("start")) {
            startStreaming(callbackContext);
            return true;
        }

        return false;
    }


    private void startStreaming(CallbackContext callbackCtx){

        callbackContext = callbackCtx;

        String msg = "";

        if (this.cordova != null) {

            // Let's check to make sure the camera is actually installed. (Legacy Nexus 7 code)
            PackageManager mPm = this.cordova.getActivity().getPackageManager();


            Context context=this.cordova.getActivity().getApplicationContext();

            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.setClass(context, cameraActivity.class);



            if(intent.resolveActivity(mPm) != null){
                cordova.startActivityForResult((CordovaPlugin) this, intent, CAMERA);

                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
                callbackContext.success("OK");
            }
            else
            {
                msg = "Error: You don't have a default camera.  Your device may not be CTS complaint";
                LOG.e(LOG_TAG, msg);
                callbackContext.error(msg);
            }
        }
        else{
            msg = "Cordova no esta disponible";
            LOG.e(LOG_TAG, msg);
            callbackContext.error(msg);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == CAMERA && callbackContext != null) {

            if (resultCode == Activity.RESULT_OK) {
                LOG.d(LOG_TAG, "*************** Activity.RESULT_OK");
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
                //callbackContext.success("OK");
            }

        } else if (resultCode == Activity.RESULT_CANCELED) {
            LOG.d(LOG_TAG, "*************** Activity.RESULT_CANCELED");
            // TODO NO_RESULT or error callback?
            //PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            //PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));

        } else {
            LOG.d(LOG_TAG, "*X*X*X*X*X*X*X*X* ERROR, resultCode == "+resultCode);
            callbackContext.error(resultCode);
        }
    }

}